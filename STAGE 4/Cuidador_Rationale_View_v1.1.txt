{
  "actors": [
    {
      "id": "6291d91a-a11b-43d2-a3d6-9273f069f3a3",
      "text": "Cuidador",
      "type": "istar.Role",
      "x": 336,
      "y": 38,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "ce868adf-9e22-4bac-87f3-dbf8005f91ce",
          "text": "Serviços publicitados",
          "type": "istar.Goal",
          "x": 494,
          "y": 58,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "36639717-1902-4571-a398-829b1a02b8d6",
          "text": "Conta registada",
          "type": "istar.Goal",
          "x": 496,
          "y": 139,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "d01ea420-83d2-440a-80ef-065de95ea200",
          "text": "Registar conta",
          "type": "istar.Task",
          "x": 492,
          "y": 218,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "ad3097c6-2c93-414c-bb09-636d8cfd1d64",
          "text": "Sem erros",
          "type": "istar.Quality",
          "x": 616,
          "y": 449,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "b4b49da0-7776-4a8c-b0e0-a047157c4dc1",
          "text": "Conta validada",
          "type": "istar.Goal",
          "x": 495,
          "y": 295,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "c10a42b0-7286-4d2d-9499-ee34c8c53bf9",
          "text": "Avaliadores validarem",
          "type": "istar.Resource",
          "x": 385,
          "y": 451,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "1cbb80c7-1af8-4d70-acde-81f8deed6573",
          "text": "Introduzir NIF",
          "type": "istar.Task",
          "x": 495,
          "y": 373,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    }
  ],
  "orphans": [],
  "dependencies": [],
  "links": [
    {
      "id": "aaf647ae-eacd-461d-98e2-e2ed707b3ba9",
      "type": "istar.AndRefinementLink",
      "source": "1cbb80c7-1af8-4d70-acde-81f8deed6573",
      "target": "b4b49da0-7776-4a8c-b0e0-a047157c4dc1"
    },
    {
      "id": "3a255206-f0d8-4564-9d75-10f738be0262",
      "type": "istar.OrRefinementLink",
      "source": "b4b49da0-7776-4a8c-b0e0-a047157c4dc1",
      "target": "d01ea420-83d2-440a-80ef-065de95ea200"
    },
    {
      "id": "75e29679-51be-4823-afd1-77666299277c",
      "type": "istar.AndRefinementLink",
      "source": "d01ea420-83d2-440a-80ef-065de95ea200",
      "target": "36639717-1902-4571-a398-829b1a02b8d6"
    },
    {
      "id": "c618402c-d99d-42a9-af6a-360b8f902343",
      "type": "istar.AndRefinementLink",
      "source": "36639717-1902-4571-a398-829b1a02b8d6",
      "target": "ce868adf-9e22-4bac-87f3-dbf8005f91ce"
    },
    {
      "id": "2bdfd353-3678-47e2-b086-f83c08f24e5f",
      "type": "istar.ContributionLink",
      "source": "d01ea420-83d2-440a-80ef-065de95ea200",
      "target": "ad3097c6-2c93-414c-bb09-636d8cfd1d64",
      "label": "help"
    },
    {
      "id": "59ad2fbb-d43f-4978-943f-bd206328f002",
      "type": "istar.NeededByLink",
      "source": "c10a42b0-7286-4d2d-9499-ee34c8c53bf9",
      "target": "1cbb80c7-1af8-4d70-acde-81f8deed6573"
    },
    {
      "id": "126f0412-3060-4cd3-8ee2-06ca87100b1e",
      "type": "istar.ContributionLink",
      "source": "1cbb80c7-1af8-4d70-acde-81f8deed6573",
      "target": "ad3097c6-2c93-414c-bb09-636d8cfd1d64",
      "label": "help"
    }
  ],
  "display": {
    "ad3097c6-2c93-414c-bb09-636d8cfd1d64": {
      "width": 81.80621337890625,
      "height": 46.84869384765625
    },
    "2bdfd353-3678-47e2-b086-f83c08f24e5f": {
      "vertices": [
        {
          "x": 629,
          "y": 279
        },
        {
          "x": 659,
          "y": 329
        },
        {
          "x": 658,
          "y": 383
        }
      ]
    }
  },
  "tool": "pistar.2.0.0",
  "istar": "2.0",
  "saveDate": "Thu, 18 Nov 2021 19:05:10 GMT",
  "diagram": {
    "width": 2000,
    "height": 1300,
    "name": "Rationale View ( Cuidador )",
    "customProperties": {
      "Description": ""
    }
  }
}