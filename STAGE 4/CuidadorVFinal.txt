{
  "actors": [
    {
      "id": "6291d91a-a11b-43d2-a3d6-9273f069f3a3",
      "text": "Cuidador",
      "type": "istar.Role",
      "x": 191,
      "y": 291,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "ce868adf-9e22-4bac-87f3-dbf8005f91ce",
          "text": "Serviços publicitados",
          "type": "istar.Goal",
          "x": 442,
          "y": 383,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "d01ea420-83d2-440a-80ef-065de95ea200",
          "text": "Registar conta",
          "type": "istar.Task",
          "x": 428,
          "y": 459,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "ad3097c6-2c93-414c-bb09-636d8cfd1d64",
          "text": "Sem erros",
          "type": "istar.Quality",
          "x": 577,
          "y": 444,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "61f7b3b0-d28c-482e-a630-cff2bcb73d55",
          "text": "Validar conta",
          "type": "istar.Task",
          "x": 426,
          "y": 556,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "47e92a65-f5dc-4dab-98a7-43de377aaf17",
          "text": "Animal acolhido",
          "type": "istar.Goal",
          "x": 695,
          "y": 533,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "d3d860a6-571f-496a-97f9-42c29f04aa85",
          "text": "Receber animal",
          "type": "istar.Task",
          "x": 805,
          "y": 313,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf",
          "text": "Introduzir NIF",
          "type": "istar.Task",
          "x": 426,
          "y": 642,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "43a12b8c-9fc5-4085-a76d-50d084e02d4f",
          "text": "Serviço pedido",
          "type": "istar.Goal",
          "x": 695,
          "y": 380,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "e3e3e283-77b0-4ec5-ab47-604ad20dda1d",
          "text": "Receber pagamento",
          "type": "istar.Task",
          "x": 809,
          "y": 433,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "52e20b1b-5330-4d27-9c73-1bd37502fe80",
          "text": "Preencher declaração",
          "type": "istar.Task",
          "x": 571,
          "y": 532,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "e2697e75-4c49-4648-bf2e-48eb365f57f5",
          "text": "Animal entregue",
          "type": "istar.Goal",
          "x": 819,
          "y": 636,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "74428296-ebf9-4ad9-90e5-cc2da5dbf5e8",
          "text": "Aumentar receita",
          "type": "istar.Goal",
          "x": 558,
          "y": 291,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "6422aaa1-581d-42be-b052-780206d80ead",
          "text": "Aumentar Alcance",
          "type": "istar.Quality",
          "x": 265,
          "y": 368,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "bbfb9c75-71d1-408c-bd09-04d3030789d9",
          "text": "Receber Critica Positiva",
          "type": "istar.Task",
          "x": 191,
          "y": 503,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "db0f3e0e-b8a8-4c7b-be6b-1fe04ca93130",
          "text": "Receber Critica Negativa",
          "type": "istar.Task",
          "x": 317,
          "y": 502,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "b2daac5a-5316-4de3-a770-f7fc1cac4d2c",
      "text": "API Website PetSitting",
      "type": "istar.Agent",
      "x": 459,
      "y": 880,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "9ce67fba-5edd-42aa-b1cc-ea681cf6692d",
      "text": "Cuidador Particular",
      "type": "istar.Role",
      "x": 116,
      "y": 116,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "c1a9482c-0604-4e09-a5e7-419fd00747fe",
      "text": "Empresa",
      "type": "istar.Role",
      "x": 245,
      "y": 112,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "f8755a51-5bc3-4827-bf0b-67683cc1fc07",
      "text": "Dono de animal",
      "type": "istar.Role",
      "x": 1366,
      "y": 631,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "76a24f3d-daaa-47ea-8376-8d033245d33b",
          "text": "Pesquisar Cuidadores",
          "type": "istar.Task",
          "x": 1426,
          "y": 715,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "232176fc-5cfd-47eb-8739-1c45e85af606",
          "text": "Pesquisar por multifiltro",
          "type": "istar.Task",
          "x": 1367,
          "y": 807,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "7b65a7b0-7876-4bd2-ad21-124ed8867e87",
          "text": "Cuidador encontrado",
          "type": "istar.Goal",
          "x": 1428,
          "y": 653,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "3f121ca9-c712-4917-8625-d92e4b879097",
          "text": "Animal Entregue",
          "type": "istar.Goal",
          "x": 1656,
          "y": 776,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "88ae1a01-7319-4519-ac20-658e964728e7",
          "text": "Animal Recebido",
          "type": "istar.Goal",
          "x": 1738,
          "y": 908,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "7db97c1c-8d6e-47a6-a900-24a96bf256ad",
          "text": "Comunicar com Cuidadores",
          "type": "istar.Task",
          "x": 1496,
          "y": 807,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "104f892f-54c8-47d5-84fe-b45ab3b1126d",
          "text": "Preecher Formulário de Registo",
          "type": "istar.Task",
          "x": 1554,
          "y": 888,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "17f40edb-4ab1-45d8-a82a-cc9a5c5bd778",
          "text": "Marcar Serviço",
          "type": "istar.Task",
          "x": 1429,
          "y": 886,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "7029f750-67f0-4468-ae23-26dc25582cec",
          "text": "Deslocar-se à morada do cuidador escolhido",
          "type": "istar.Task",
          "x": 1636,
          "y": 647,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "45aacd89-84b5-430b-b94d-e26a84bc40e2",
          "text": "Entregar Formulário",
          "type": "istar.Task",
          "x": 1876,
          "y": 666,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "a3c4c5d7-53c6-4798-9925-6a82949ad366",
          "text": "Receber Declaração",
          "type": "istar.Task",
          "x": 1760,
          "y": 656,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "eb0c2088-80f3-4b8a-8706-c2530d4074be",
          "text": "Efetuar Pagamento",
          "type": "istar.Task",
          "x": 1538,
          "y": 666,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "9fa8ceaa-8ef1-4296-999b-10431f46e69c",
          "text": "Escrever Crítica",
          "type": "istar.Task",
          "x": 1875,
          "y": 912,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "88fa0c2b-36cb-4eaf-95d5-b7a166796a13",
      "text": "Avaliador",
      "type": "istar.Role",
      "x": 123,
      "y": 856,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    }
  ],
  "orphans": [],
  "dependencies": [
    {
      "id": "9a0a9b03-dcfe-4b5b-ad2b-01e7cf770f11",
      "text": "NIF",
      "type": "istar.Resource",
      "x": 355,
      "y": 749,
      "customProperties": {
        "Description": ""
      },
      "source": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf",
      "target": "b2daac5a-5316-4de3-a770-f7fc1cac4d2c"
    },
    {
      "id": "53fd3212-eb7f-48ac-91f4-016be7af1334",
      "text": "NIF validado",
      "type": "istar.Goal",
      "x": 503,
      "y": 751,
      "customProperties": {
        "Description": ""
      },
      "source": "b2daac5a-5316-4de3-a770-f7fc1cac4d2c",
      "target": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf"
    },
    {
      "id": "cfefb6dc-67a8-48fd-ad0d-7e320c1dbe08",
      "text": "Entregar Animal",
      "type": "istar.Task",
      "x": 1006,
      "y": 672,
      "customProperties": {
        "Description": ""
      },
      "source": "e2697e75-4c49-4648-bf2e-48eb365f57f5",
      "target": "88ae1a01-7319-4519-ac20-658e964728e7"
    },
    {
      "id": "91c2d369-6b4a-496f-8f49-7fd546a140e0",
      "text": "Declaração",
      "type": "istar.Task",
      "x": 995,
      "y": 568,
      "customProperties": {
        "Description": ""
      },
      "source": "52e20b1b-5330-4d27-9c73-1bd37502fe80",
      "target": "a3c4c5d7-53c6-4798-9925-6a82949ad366"
    },
    {
      "id": "a45ff141-8fae-4f5a-acf7-4b7bc848db16",
      "text": "Formulário de Registo",
      "type": "istar.Resource",
      "x": 1113,
      "y": 303,
      "customProperties": {
        "Description": ""
      },
      "source": "45aacd89-84b5-430b-b94d-e26a84bc40e2",
      "target": "d3d860a6-571f-496a-97f9-42c29f04aa85"
    },
    {
      "id": "db6a1667-de6b-41e1-9039-e9358fc8c25f",
      "text": "Animal",
      "type": "istar.Resource",
      "x": 1080,
      "y": 407,
      "customProperties": {
        "Description": ""
      },
      "source": "7029f750-67f0-4468-ae23-26dc25582cec",
      "target": "d3d860a6-571f-496a-97f9-42c29f04aa85"
    },
    {
      "id": "62fa99fe-8d1e-4d29-b5b1-3e2935c4a508",
      "text": "Efetuar Transferência",
      "type": "istar.Task",
      "x": 1026,
      "y": 478,
      "customProperties": {
        "Description": ""
      },
      "source": "eb0c2088-80f3-4b8a-8706-c2530d4074be",
      "target": "e3e3e283-77b0-4ec5-ab47-604ad20dda1d"
    },
    {
      "id": "fc180a0e-9e3b-49be-a77f-f599cc322131",
      "text": "Atribuir selo de qualidade",
      "type": "istar.Task",
      "x": 89,
      "y": 674,
      "customProperties": {
        "Description": ""
      },
      "source": "88fa0c2b-36cb-4eaf-95d5-b7a166796a13",
      "target": "bbfb9c75-71d1-408c-bd09-04d3030789d9"
    },
    {
      "id": "96f39a99-cfd7-40c0-95d6-41aa4ed287b2",
      "text": "Banir cuidador",
      "type": "istar.Task",
      "x": 226,
      "y": 712,
      "customProperties": {
        "Description": ""
      },
      "source": "88fa0c2b-36cb-4eaf-95d5-b7a166796a13",
      "target": "db0f3e0e-b8a8-4c7b-be6b-1fe04ca93130"
    }
  ],
  "links": [
    {
      "id": "6694de17-276d-4634-8daf-a9232fb3e01c",
      "type": "istar.DependencyLink",
      "source": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf",
      "target": "9a0a9b03-dcfe-4b5b-ad2b-01e7cf770f11"
    },
    {
      "id": "b2baed6d-0b63-4d61-8c37-4798bf02e954",
      "type": "istar.DependencyLink",
      "source": "9a0a9b03-dcfe-4b5b-ad2b-01e7cf770f11",
      "target": "b2daac5a-5316-4de3-a770-f7fc1cac4d2c"
    },
    {
      "id": "0c51981a-ee39-4c5d-8f31-dae3335ca7b4",
      "type": "istar.DependencyLink",
      "source": "b2daac5a-5316-4de3-a770-f7fc1cac4d2c",
      "target": "53fd3212-eb7f-48ac-91f4-016be7af1334"
    },
    {
      "id": "8193796f-9bd4-4e90-aba9-b6636ab5e7ed",
      "type": "istar.DependencyLink",
      "source": "53fd3212-eb7f-48ac-91f4-016be7af1334",
      "target": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf"
    },
    {
      "id": "4b25cb31-a356-48dc-a02a-7c49a4c844c1",
      "type": "istar.DependencyLink",
      "source": "e2697e75-4c49-4648-bf2e-48eb365f57f5",
      "target": "cfefb6dc-67a8-48fd-ad0d-7e320c1dbe08"
    },
    {
      "id": "1c555e2c-9e9a-4d62-bdcb-fca8dae599ba",
      "type": "istar.DependencyLink",
      "source": "cfefb6dc-67a8-48fd-ad0d-7e320c1dbe08",
      "target": "88ae1a01-7319-4519-ac20-658e964728e7"
    },
    {
      "id": "26182129-0dcb-43bf-8052-e12b6b979c49",
      "type": "istar.DependencyLink",
      "source": "52e20b1b-5330-4d27-9c73-1bd37502fe80",
      "target": "91c2d369-6b4a-496f-8f49-7fd546a140e0"
    },
    {
      "id": "ba78a3ee-a885-454c-ac08-4f7adfd6a44d",
      "type": "istar.DependencyLink",
      "source": "91c2d369-6b4a-496f-8f49-7fd546a140e0",
      "target": "a3c4c5d7-53c6-4798-9925-6a82949ad366"
    },
    {
      "id": "648c608d-36d9-4442-b97c-970f2f9e9db8",
      "type": "istar.DependencyLink",
      "source": "45aacd89-84b5-430b-b94d-e26a84bc40e2",
      "target": "a45ff141-8fae-4f5a-acf7-4b7bc848db16"
    },
    {
      "id": "e5ac2f4f-d9f9-4b03-908b-08c45ccc26dc",
      "type": "istar.DependencyLink",
      "source": "a45ff141-8fae-4f5a-acf7-4b7bc848db16",
      "target": "d3d860a6-571f-496a-97f9-42c29f04aa85"
    },
    {
      "id": "6749df82-db02-450e-b30c-e29c5176bcdb",
      "type": "istar.DependencyLink",
      "source": "7029f750-67f0-4468-ae23-26dc25582cec",
      "target": "db6a1667-de6b-41e1-9039-e9358fc8c25f"
    },
    {
      "id": "529ed168-dc82-4b10-a519-85b3b8905a6c",
      "type": "istar.DependencyLink",
      "source": "db6a1667-de6b-41e1-9039-e9358fc8c25f",
      "target": "d3d860a6-571f-496a-97f9-42c29f04aa85"
    },
    {
      "id": "047a3252-488d-4359-931a-fddfd764b91a",
      "type": "istar.DependencyLink",
      "source": "eb0c2088-80f3-4b8a-8706-c2530d4074be",
      "target": "62fa99fe-8d1e-4d29-b5b1-3e2935c4a508"
    },
    {
      "id": "f273aa01-ee00-4031-a3e7-d10af11c8c6e",
      "type": "istar.DependencyLink",
      "source": "62fa99fe-8d1e-4d29-b5b1-3e2935c4a508",
      "target": "e3e3e283-77b0-4ec5-ab47-604ad20dda1d"
    },
    {
      "id": "ab75535d-4977-444f-bffe-1e9585f26957",
      "type": "istar.ContributionLink",
      "source": "d01ea420-83d2-440a-80ef-065de95ea200",
      "target": "ad3097c6-2c93-414c-bb09-636d8cfd1d64",
      "label": "help"
    },
    {
      "id": "02f06e29-d31e-40c8-a33e-0a5231b39d81",
      "type": "istar.OrRefinementLink",
      "source": "61f7b3b0-d28c-482e-a630-cff2bcb73d55",
      "target": "d01ea420-83d2-440a-80ef-065de95ea200"
    },
    {
      "id": "1c5c5dcc-a101-48be-9295-becc4b098d29",
      "type": "istar.AndRefinementLink",
      "source": "d01ea420-83d2-440a-80ef-065de95ea200",
      "target": "ce868adf-9e22-4bac-87f3-dbf8005f91ce"
    },
    {
      "id": "54cd28a2-0101-4dcb-93dc-740287ec0914",
      "type": "istar.IsALink",
      "source": "c1a9482c-0604-4e09-a5e7-419fd00747fe",
      "target": "6291d91a-a11b-43d2-a3d6-9273f069f3a3"
    },
    {
      "id": "9c88e965-b7e7-4a31-816f-0a86556a0e1a",
      "type": "istar.IsALink",
      "source": "9ce67fba-5edd-42aa-b1cc-ea681cf6692d",
      "target": "6291d91a-a11b-43d2-a3d6-9273f069f3a3"
    },
    {
      "id": "863c15b3-b1a1-4892-8864-60015b2cced1",
      "type": "istar.AndRefinementLink",
      "source": "d3d860a6-571f-496a-97f9-42c29f04aa85",
      "target": "47e92a65-f5dc-4dab-98a7-43de377aaf17"
    },
    {
      "id": "86b49545-4d02-409e-a1ce-4822538bf29c",
      "type": "istar.AndRefinementLink",
      "source": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf",
      "target": "61f7b3b0-d28c-482e-a630-cff2bcb73d55"
    },
    {
      "id": "1e3ed76c-ef36-490d-b9b1-cb1657961854",
      "type": "istar.AndRefinementLink",
      "source": "47e92a65-f5dc-4dab-98a7-43de377aaf17",
      "target": "43a12b8c-9fc5-4085-a76d-50d084e02d4f"
    },
    {
      "id": "6632f7b3-d29a-4836-9c05-9876f86d09c9",
      "type": "istar.AndRefinementLink",
      "source": "e3e3e283-77b0-4ec5-ab47-604ad20dda1d",
      "target": "47e92a65-f5dc-4dab-98a7-43de377aaf17"
    },
    {
      "id": "e99a5432-a367-4f05-a45a-0294562e13fa",
      "type": "istar.AndRefinementLink",
      "source": "52e20b1b-5330-4d27-9c73-1bd37502fe80",
      "target": "47e92a65-f5dc-4dab-98a7-43de377aaf17"
    },
    {
      "id": "cf00775f-ed71-431e-8bff-ec6a07f58b2c",
      "type": "istar.ContributionLink",
      "source": "52e20b1b-5330-4d27-9c73-1bd37502fe80",
      "target": "ad3097c6-2c93-414c-bb09-636d8cfd1d64",
      "label": "hurt"
    },
    {
      "id": "9892011d-c944-497c-9e6e-5876f0318999",
      "type": "istar.AndRefinementLink",
      "source": "ce868adf-9e22-4bac-87f3-dbf8005f91ce",
      "target": "74428296-ebf9-4ad9-90e5-cc2da5dbf5e8"
    },
    {
      "id": "cfe9cd3c-1410-4797-8980-021d281cf719",
      "type": "istar.AndRefinementLink",
      "source": "43a12b8c-9fc5-4085-a76d-50d084e02d4f",
      "target": "74428296-ebf9-4ad9-90e5-cc2da5dbf5e8"
    },
    {
      "id": "520d83ff-284e-493c-b576-f172f1b24e78",
      "type": "istar.ContributionLink",
      "source": "ce868adf-9e22-4bac-87f3-dbf8005f91ce",
      "target": "6422aaa1-581d-42be-b052-780206d80ead",
      "label": "help"
    },
    {
      "id": "dcebbeb1-fd50-41d7-8308-86de47b1febf",
      "type": "istar.ContributionLink",
      "source": "bbfb9c75-71d1-408c-bd09-04d3030789d9",
      "target": "6422aaa1-581d-42be-b052-780206d80ead",
      "label": "help"
    },
    {
      "id": "7aeefe87-4ed8-4904-86ae-9d56ff9a48b4",
      "type": "istar.ContributionLink",
      "source": "db0f3e0e-b8a8-4c7b-be6b-1fe04ca93130",
      "target": "6422aaa1-581d-42be-b052-780206d80ead",
      "label": "hurt"
    },
    {
      "id": "3c27babb-1155-4740-8a8c-6825b7e2ff9a",
      "type": "istar.AndRefinementLink",
      "source": "17f40edb-4ab1-45d8-a82a-cc9a5c5bd778",
      "target": "7db97c1c-8d6e-47a6-a900-24a96bf256ad"
    },
    {
      "id": "27035242-2614-4a60-a72d-ffbb1b5a0d00",
      "type": "istar.AndRefinementLink",
      "source": "104f892f-54c8-47d5-84fe-b45ab3b1126d",
      "target": "7db97c1c-8d6e-47a6-a900-24a96bf256ad"
    },
    {
      "id": "711b6e0e-f2cb-4aa9-906b-6c48ef5460e1",
      "type": "istar.AndRefinementLink",
      "source": "232176fc-5cfd-47eb-8739-1c45e85af606",
      "target": "76a24f3d-daaa-47ea-8376-8d033245d33b"
    },
    {
      "id": "425ed1ba-00e7-4199-81d6-5edd3aa43434",
      "type": "istar.AndRefinementLink",
      "source": "7db97c1c-8d6e-47a6-a900-24a96bf256ad",
      "target": "76a24f3d-daaa-47ea-8376-8d033245d33b"
    },
    {
      "id": "c3f0ddf7-97be-4d7e-a49c-eff6fece9568",
      "type": "istar.AndRefinementLink",
      "source": "76a24f3d-daaa-47ea-8376-8d033245d33b",
      "target": "7b65a7b0-7876-4bd2-ad21-124ed8867e87"
    },
    {
      "id": "03885d4d-47f8-4782-836c-e54bdedd8c29",
      "type": "istar.AndRefinementLink",
      "source": "7029f750-67f0-4468-ae23-26dc25582cec",
      "target": "3f121ca9-c712-4917-8625-d92e4b879097"
    },
    {
      "id": "c4f3c2ae-97eb-4f59-8f6f-24811aa6e1ff",
      "type": "istar.AndRefinementLink",
      "source": "45aacd89-84b5-430b-b94d-e26a84bc40e2",
      "target": "3f121ca9-c712-4917-8625-d92e4b879097"
    },
    {
      "id": "048a6376-a864-4ace-92a8-1cf18d789176",
      "type": "istar.AndRefinementLink",
      "source": "a3c4c5d7-53c6-4798-9925-6a82949ad366",
      "target": "3f121ca9-c712-4917-8625-d92e4b879097"
    },
    {
      "id": "e190c881-5316-41f2-8ec4-98e5bd26e482",
      "type": "istar.AndRefinementLink",
      "source": "eb0c2088-80f3-4b8a-8706-c2530d4074be",
      "target": "3f121ca9-c712-4917-8625-d92e4b879097"
    },
    {
      "id": "b6cfda08-71b3-48ad-abb5-43cd68d4b8b7",
      "type": "istar.OrRefinementLink",
      "source": "9fa8ceaa-8ef1-4296-999b-10431f46e69c",
      "target": "88ae1a01-7319-4519-ac20-658e964728e7"
    },
    {
      "id": "1fe2d0ba-d23a-4b43-97b8-f57971cecf60",
      "type": "istar.DependencyLink",
      "source": "88fa0c2b-36cb-4eaf-95d5-b7a166796a13",
      "target": "fc180a0e-9e3b-49be-a77f-f599cc322131"
    },
    {
      "id": "f2ab1f72-2182-4ecc-93b9-9db804b2edb5",
      "type": "istar.DependencyLink",
      "source": "fc180a0e-9e3b-49be-a77f-f599cc322131",
      "target": "bbfb9c75-71d1-408c-bd09-04d3030789d9"
    },
    {
      "id": "d5dcf591-2490-4129-9ae4-16ff6489a742",
      "type": "istar.DependencyLink",
      "source": "88fa0c2b-36cb-4eaf-95d5-b7a166796a13",
      "target": "96f39a99-cfd7-40c0-95d6-41aa4ed287b2"
    },
    {
      "id": "0a75d1cb-aa17-4fd1-8ce3-88403132e707",
      "type": "istar.DependencyLink",
      "source": "96f39a99-cfd7-40c0-95d6-41aa4ed287b2",
      "target": "db0f3e0e-b8a8-4c7b-be6b-1fe04ca93130"
    }
  ],
  "display": {
    "ad3097c6-2c93-414c-bb09-636d8cfd1d64": {
      "width": 81.80621337890625,
      "height": 46.84869384765625
    },
    "7029f750-67f0-4468-ae23-26dc25582cec": {
      "width": 108.88333129882812,
      "height": 61
    },
    "26182129-0dcb-43bf-8052-e12b6b979c49": {
      "vertices": [
        {
          "x": 625,
          "y": 598
        },
        {
          "x": 703,
          "y": 610
        }
      ]
    },
    "b2daac5a-5316-4de3-a770-f7fc1cac4d2c": {
      "collapsed": true
    },
    "9ce67fba-5edd-42aa-b1cc-ea681cf6692d": {
      "collapsed": true
    },
    "c1a9482c-0604-4e09-a5e7-419fd00747fe": {
      "collapsed": true
    },
    "f8755a51-5bc3-4827-bf0b-67683cc1fc07": {
      "collapsed": true
    },
    "88fa0c2b-36cb-4eaf-95d5-b7a166796a13": {
      "collapsed": true
    }
  },
  "tool": "pistar.2.0.0",
  "istar": "2.0",
  "saveDate": "Sat, 20 Nov 2021 20:13:19 GMT",
  "diagram": {
    "width": 2000,
    "height": 1649,
    "name": "Rationale View ( Cuidador )",
    "customProperties": {
      "Description": ""
    }
  }
}